/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package warlox.armory.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Tests for the Backend. Tests items, players and matches Before the tests are
 * run all database tables are cleaned before each test the Item table is
 * initiated with some items after each test the tables are cleaned and after
 * all tests our database is initiated to our frontend
 */
public class TestBasics {

    static IBackend backend;
    final static String PU = "warlox_pu";

    @BeforeClass
    public static void beforeClass() {
        //Empty all tables.
        backend = BackendFactory.getBackend(PU);

        IPlayerRegistry pr = backend.getPlayerRegistry();
        IItemCatalogue ic = backend.getItemCatalogue();
        IMatchRegistry mr = backend.getMatchRegistry();

        mr.removeAll();
        pr.removeAll();
        ic.removeAll();
    }

     /*This method was implemented so that we would have data in our tables for 
     *the frontend. The purpose was that after all tests has been run, 
     *there will be some players, items, and matches in the database for 
     *the frontend. If you want to have data while running the frontend
     *uncomment the next section. Else the frontend will have add entities to 
     *the database itself, which for us is done in the admin page.
     */
    @AfterClass
    public static void initTablesToFrontend() {
        Random random = new Random();
        IItemCatalogue ic = backend.getItemCatalogue();

        //Adding default items to the ItemCatalogue
        Item naked_helm = new Item("naked", Item.Slot.HEAD, "naked.png", 0, 0, 0, 0);
        ic.add(naked_helm);
        Item naked_armor = new Item("naked", Item.Slot.CHEST, "naked.png", 0, 0, 0, 0);
        ic.add(naked_armor);
        Item naked_boots = new Item("naked", Item.Slot.FEET, "naked.png", 0, 0, 0, 0);
        ic.add(naked_boots);
        Item naked_gloves = new Item("naked", Item.Slot.HANDS, "naked", 0, 0, 0, 0);
        ic.add(naked_gloves);
        Item i1 = new Item("Panda Hat 3", Item.Slot.HEAD, "pandahat.png", 5, 5, 0, 0);
        ic.add(i1);
        i1 = new Item("Panda Armor", Item.Slot.CHEST, "pandachest.png", 10, 2, 5, 5);
        ic.add(i1);        
        i1 = new Item("Edvards Mössa", Item.Slot.HEAD, "edvards_mossa.png", 0, 0, 10, 0);
        ic.add(i1);
        i1 = new Item("Gustavs Pecks", Item.Slot.CHEST, "elvenarmor.png", 5, 5, -5, -5);
        ic.add(i1);
        i1 = new Item("Bröds Ninjaskjorta", Item.Slot.CHEST, "ninjashirt.png", 10, 2, 5, 5);
        ic.add(i1);
        i1 = new Item("Magma Boots", Item.Slot.FEET, "magmaboots.png", -5, 15, 5, -5);
        ic.add(i1);
        i1 = new Item("Elven Gloves", Item.Slot.HANDS, "elvglove", 0, 2, 10, -1);
        ic.add(i1);
        i1 = new Item("Bröds Ninjavantar", Item.Slot.HANDS, "ninjaglove", 5, 2, 10, 4);
        ic.add(i1);
        i1 = new Item("Bröds Ninjatofflor", Item.Slot.FEET, "ninjaslippers.png", 5, 5, 5, 5);
        ic.add(i1);


        IPlayerRegistry pr = backend.getPlayerRegistry();
        //Adding some players
        for (int i = 0; i < 20; i++) {
            pr.add(new Player(new Stats(1000), "player" + i, "mail" + "i" + "@mail.com"));
        }

        Player p1 = (new Player(new Stats(1000), "Knuss", "knusse@mailmail.com"));
        pr.add(p1);
        Player p2 = new Player(new Stats(1000), "Bredvard", "bredde@mailmail.com");
        pr.add(p2);
        pr.add(new Player(new Stats(1000), "örna", "orn@eagle.base"));
        pr.add(new Player(new Stats(1000), "purvi", "parv@parviparvi.fi"));
        p1.setArmor(ic.getByNameAndSlot("Bröds Ninjaskjorta", Item.Slot.CHEST));
        p1.setGloves(ic.getByNameAndSlot("Bröds Ninjavantar", Item.Slot.HANDS));
        p1.setBoots(ic.getByNameAndSlot("Bröds Ninjatofflor", Item.Slot.FEET));

        pr.update(p1);
        p2.setHelm(ic.getByNameAndSlot("Panda Hat 3", Item.Slot.HEAD));
        p2.setGloves(ic.getByNameAndSlot("Elven Gloves", Item.Slot.HANDS));
        p2.setArmor(ic.getByNameAndSlot("Bröds Ninjaskjorta", Item.Slot.CHEST));
        p2.setBoots(ic.getByNameAndSlot("Magma Boots", Item.Slot.FEET));
        pr.update(p2);
  

        IMatchRegistry mr = backend.getMatchRegistry();
        List<PlayerMatchStats> stats;
        int matches = 20;
        for (int i = 0; i < matches; i++) {
            stats = new ArrayList<>();
            stats.add(new PlayerMatchStats(pr.getByName("Knuss").get(0), random.nextInt(5), random.nextInt(5), random.nextInt(5)));
            stats.add(new PlayerMatchStats(pr.getByName("Bredvard").get(0), random.nextInt(5), random.nextInt(4), random.nextInt(5)));
            stats.add(new PlayerMatchStats(pr.getByName("örna").get(0), random.nextInt(5), random.nextInt(5), random.nextInt(5)));
            stats.add(new PlayerMatchStats(pr.getByName("purvi").get(0), random.nextInt(5), random.nextInt(5), random.nextInt(5)));
            Game_Match m = new Game_Match(stats, pr);
            mr.add(m);
        }
    }

    @Before // Run before each test
    public void before() {
        IItemCatalogue ic = backend.getItemCatalogue();

        //Adding default items to the ItemCatalogue, all slots should atleast have a naked
        Item naked_helm = new Item("naked", Item.Slot.HEAD, "naked.png", 0, 0, 0, 0);
        ic.add(naked_helm);
        Item naked_armor = new Item("naked", Item.Slot.CHEST, "naked.png", 0, 0, 0, 0);
        ic.add(naked_armor);
        Item naked_boots = new Item("naked", Item.Slot.FEET, "naked.png", 0, 0, 0, 0);
        ic.add(naked_boots);
        Item naked_gloves = new Item("naked", Item.Slot.HANDS, "naked", 0, 0, 0, 0);
        ic.add(naked_gloves);
    }

    @After
    public void after() {
        IPlayerRegistry pr = backend.getPlayerRegistry();
        IItemCatalogue ic = backend.getItemCatalogue();
        IMatchRegistry mr = backend.getMatchRegistry();

        mr.removeAll();
        pr.removeAll();
        ic.removeAll();
    }

    @Test
    public void testItems() {
        /*
         * Starting "add/update/remove item" tests
         */

        IItemCatalogue ic = backend.getItemCatalogue();
        int itemCountBefore = ic.getCount();
        Item i1 = new Item("Panda Hat 3", Item.Slot.HEAD, "pandahat.png", 0, 0, 0, 0);
        ic.add(i1);

        i1 = new Item("Edvards Mössa", Item.Slot.HEAD, "edvards_mossa.png", 0, 0, 100000, 0);
        ic.add(i1);
        i1 = new Item("Gustavs Pecks", Item.Slot.CHEST, "null", 0, 0, 0, 0);
        ic.add(i1);

        // Has three new items been added
        assertTrue(ic.getCount() == itemCountBefore + 3);


        Item i2 = ic.find(i1.getId());

        // Not same transaction
        assertTrue(i2 != i1);
        // Equal by value
        assertTrue(i2.equals(i1));

        Item i = new Item(i1.getId(), "updated", i1.getSlot(), "null", 0, 0, 0, 0);
        i1 = ic.update(i);

        assertTrue(i1.equals(i2));
        assertFalse(i2.getName().equals(i1.getName()));

        itemCountBefore = ic.getCount();
        ic.remove(i1.getId());
        assertTrue(ic.getCount() == itemCountBefore - 1);
        assertTrue(ic.find(i1.getId()) == null);

        // No change in program (but deleted from database)
        assertTrue(i2.equals(i1));
        assertFalse(i2.getName().equals(i1.getName()));
        //---End of add/update/remove item tests---

    }

    @Test
    public void testGetRangeOfItems() {
        /*
         * Testing getRange
         */
        IItemCatalogue ic = backend.getItemCatalogue();
        List<Item> ps = ic.getRange(0, 2);
        assertTrue(ps.size() == 2);
        //---End of getRange test---
    }

    @Test
    public void testGetItemByName() {
        /*
         * Testing "get item by name"
         */
        IItemCatalogue ic = backend.getItemCatalogue();
        Item i1 = new Item("Panda Hat 3", Item.Slot.HEAD, "pandahat.png", 0, 0, 0, 0);
        ic.add(i1);

        List<Item> is = ic.getByName("Panda Hat 3");
        assertTrue(is.size() == 1);
        assertTrue(is.get(0).getName().equalsIgnoreCase("Panda Hat 3"));
        //---End of "get item by name" test---
    }

    @Test
    public void testPlayers() {
        IPlayerRegistry pr = backend.getPlayerRegistry();
        IItemCatalogue ic = backend.getItemCatalogue();

        //Adding two players to the PlayerRegistry to be used in this test

        Player pl = new Player(new Stats(500000), "Knuss", "knusse@mailmail.com");
        pr.add(pl);

        Player p2 = new Player(new Stats(6), "Bredvard", "bredde@mailmail.com");
        pr.add(p2);

        //Check that a freshplayer has no gear
        List<Player> players = pr.getRange(0, pr.getCount());
        for (Player p : players) {
            assertTrue(p.getHelm().equals(ic.getByNameAndSlot("naked", Item.Slot.HEAD))
                    && p.getArmor().equals(ic.getByNameAndSlot("naked", Item.Slot.CHEST))
                    && p.getGloves().equals(ic.getByNameAndSlot("naked", Item.Slot.HANDS))
                    && p.getBoots().equals(ic.getByNameAndSlot("naked", Item.Slot.FEET)));
        }

        //Add three new items to the itemcatalogue
        Item i1 = new Item("Panda Hat 3", Item.Slot.HEAD, "pandahat.png", 0, 0, 0, 0);
        ic.add(i1);
        i1 = new Item("Edvards Mössa", Item.Slot.HEAD, "edvards_mossa.png", 0, 0, 100000, 0);
        ic.add(i1);
        i1 = new Item("Gustavs Pecks", Item.Slot.CHEST, "null", 0, 0, 0, 0);
        ic.add(i1);

        //Equip a helm on p1 and assert correct behavior
        pl.setHelm(ic.getByNameAndSlot("Edvards Mössa", Item.Slot.HEAD));
        pr.update(pl);
        pl = pr.find(pl.getId());
        assertTrue(pl.getHelm().equals(ic.getByNameAndSlot("Edvards Mössa", Item.Slot.HEAD))
                && pl.getArmor().equals(ic.getByNameAndSlot("naked", Item.Slot.CHEST))
                && pl.getGloves().equals(ic.getByNameAndSlot("naked", Item.Slot.HANDS))
                && pl.getBoots().equals(ic.getByNameAndSlot("naked", Item.Slot.FEET)));

        //Equip a helm and chest on p2 and assert correct behavior
        p2.setHelm(ic.getByNameAndSlot("Panda Hat 3", Item.Slot.HEAD));
        p2.setArmor(ic.getByNameAndSlot("Gustavs Pecks", Item.Slot.CHEST));
        pr.update(p2);
        assertTrue(p2.getHelm().equals(ic.getByNameAndSlot("Panda Hat 3", Item.Slot.HEAD))
                && p2.getArmor().equals(ic.getByNameAndSlot("Gustavs Pecks", Item.Slot.CHEST))
                && p2.getGloves().equals(ic.getByNameAndSlot("naked", Item.Slot.HANDS))
                && p2.getBoots().equals(ic.getByNameAndSlot("naked", Item.Slot.FEET)));

    }

    @Test
    public void testMatches() {
        IPlayerRegistry pr = backend.getPlayerRegistry();
        IMatchRegistry mr = backend.getMatchRegistry();

        /*
         * Create a game with 2 players. Validate that one game
         * has been created.
         */

        Player pl = new Player(new Stats(500000), "Knuss", "knusse@mailmail.com");
        pr.add(pl);

        Player p2 = new Player(new Stats(6), "Bredvard", "bredde@mailmail.com");
        pr.add(p2);

        List<PlayerMatchStats> stats = new ArrayList<>();
        stats.add(new PlayerMatchStats(pr.getByName("Knuss").get(0), 0, 10000, 0));
        stats.add(new PlayerMatchStats(pr.getByName("Bredvard").get(0), 100000, 0, 0));
        Game_Match m = new Game_Match(stats, pr);
        mr.add(m);
        assertTrue(mr.getCount() == 1);

        /*
         * Create another game, this time with only one player.
         * Validate that two games have been saved, and that Knuss
         * has participated in one game while Bredvard has 
         * participated in two.
         */
        stats = new ArrayList<>();
        stats.add(new PlayerMatchStats(pr.getByName("Bredvard").get(0), 100000, 0, 0));
        m = new Game_Match(stats, pr);
        mr.add(m);

        assertTrue(mr.getCount() == 2);
        assertTrue(mr.getByPlayer(pr.getByName("Knuss").get(0), Integer.MAX_VALUE).size() == 1);
        assertTrue(mr.getByPlayer(pr.getByName("Bredvard").get(0), Integer.MAX_VALUE).size() == 2);
    }

    @Test
    public void testSortUserName() {
        IPlayerRegistry pr = backend.getPlayerRegistry();
        Player pl = new Player(new Stats(500000), "Knuss", "knusse@mailmail.com");
        pr.add(pl);

        Player p2 = new Player(new Stats(6), "Bredvard", "bredde@mailmail.com");
        pr.add(p2);

        EntityManagerFactory emf = Persistence.createEntityManagerFactory(PU);
        EntityManager em = emf.createEntityManager();
        Query query = em.createNamedQuery("Player.sortByName");

        Collection players = query.getResultList();

        Iterator iterator = players.iterator();
        pl = (Player) iterator.next();

        while (iterator.hasNext()) {
            p2 = (Player) iterator.next();
            assertTrue(pl.getUserName().compareTo(p2.getUserName()) < 0);
            pl = p2;
        }
    }

    @Test
    public void testSortRating() {
        IPlayerRegistry pr = backend.getPlayerRegistry();
        Player pl = new Player(new Stats(500000), "Knuss", "knusse@mailmail.com");
        pr.add(pl);

        Player p2 = new Player(new Stats(6), "Bredvard", "bredde@mailmail.com");
        pr.add(p2);

        EntityManagerFactory emf = Persistence.createEntityManagerFactory(PU);
        EntityManager em = emf.createEntityManager();
        Query query = em.createNamedQuery("Player.sortByRating");

        Collection players = query.getResultList();

        Iterator iterator = players.iterator();
        pl = (Player) iterator.next();

        while (iterator.hasNext()) {
            p2 = (Player) iterator.next();
            assertTrue(pl.getStats().getRating() > p2.getStats().getRating());
            pl = p2;
        }
    }
}
