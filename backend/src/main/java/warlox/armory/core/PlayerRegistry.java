package warlox.armory.core;

import warlox.armory.db.AbstractDAO;
import java.util.ArrayList;
import java.util.List;

/**
 * All players
 */
public final class PlayerRegistry extends AbstractDAO<Player, Long>
        implements IPlayerRegistry {

    public static IPlayerRegistry newInstance(String puName) {
        return new PlayerRegistry(puName);
    }

    public PlayerRegistry(String puName) {
        super(Player.class, puName);
    }

    @Override
    public List<Player> getByName(String name) {
        List<Player> found = new ArrayList<>();
        for (Player c : getRange(0, getCount())) {
            if (c.getUserName().equals(name)) {
                found.add(c);
            }
        }
        return found;
    }

    //Returns all players with a name containing the input parameter.
    public List<Player> getBySubName(String name) {
        name = name.toLowerCase();
        List<Player> found = new ArrayList<>();
        for (Player c : getRange(0, getCount())) {
            if (c.getUserName().toLowerCase().contains(name)) {
                found.add(c);
            }
        }
        return found;
    }
        
    @Override
    public void removeAll() {
        for (Player p : getRange(0, getCount())) {
            remove(p.getId());
        }
    }
}
