package warlox.armory.core;

import warlox.armory.db.AbstractDAO;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * All matches
 *
 */
public final class MatchRegistry extends AbstractDAO<Game_Match, Long>
        implements IMatchRegistry {

    public static IMatchRegistry newInstance(String puName) {
        return new MatchRegistry(puName);
    }

    public MatchRegistry(String puName) {
        super(Game_Match.class, puName);
    }

    //This method returns all games a player has participated in.
    @Override
    public List<Game_Match> getByPlayer(Player player, int maxGames) {
        List<Game_Match> found = new ArrayList<>();
        List<Game_Match> allGames = getRange(0, getCount());
        int foundGames = 0;
        Collections.reverse(allGames);
        for (Game_Match m : allGames) {
            if (m.containsPlayer(player)) {
                found.add(m);
                foundGames++;
                if (foundGames >= maxGames) {
                    break;
                }
            }
        }
        return found;
    }

    @Override
    public void removeAll() {
        for (Game_Match p : getRange(0, getCount())) {
            remove(p.getId());
        }
    }
}
