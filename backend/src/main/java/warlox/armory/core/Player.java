package warlox.armory.core;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * A Player
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Player.sortByName",
            query = "SELECT c FROM Player c ORDER BY c.userName"),
    @NamedQuery(name = "Player.sortByRating",
            query = "SELECT c FROM Player c ORDER BY c.stats.rating DESC")
})
public class Player extends AbstractEntity {

    private String userName;
    private String email;
    @Embedded
    private Stats stats;
    @ManyToOne
    private Item item_headGear;
    @ManyToOne
    private Item item_armor;
    @ManyToOne
    private Item item_gloves;
    @ManyToOne
    private Item item_boots;

    public Player() {
    }

    public Player(Stats stats, String userName, String email) {
        IBackend backend = BackendFactory.getBackend("warlox_pu");
        this.stats = stats;
        this.userName = userName;
        this.email = email;
        this.item_headGear = backend.getItemCatalogue().getByNameAndSlot("naked", Item.Slot.HEAD);
        this.item_armor = backend.getItemCatalogue().getByNameAndSlot("naked", Item.Slot.CHEST);
        this.item_gloves = backend.getItemCatalogue().getByNameAndSlot("naked", Item.Slot.HANDS);
        this.item_boots = backend.getItemCatalogue().getByNameAndSlot("naked", Item.Slot.FEET);
    }

    public void setHelm(Item item) {
        Logger.getAnonymousLogger().log(Level.INFO, "Equipping " + item.getName() + " to head slot.", this.hashCode());
        if (item.getSlot() != Item.Slot.HEAD) {
            Logger.getAnonymousLogger().log(Level.INFO, "Cannot equip " + item.getName() + " to head slot.", this.hashCode());
        }
        item_headGear = item;
    }

    public void setArmor(Item item) {
        Logger.getAnonymousLogger().log(Level.INFO, "Equipping " + item.getName() + " to chest slot.", this.hashCode());
        if (item.getSlot() != Item.Slot.CHEST) {
            Logger.getAnonymousLogger().log(Level.INFO, "Cannot equip " + item.getName() + " to chest slot.", this.hashCode());
        }
        item_armor = item;
    }

    public void setBoots(Item item) {
        Logger.getAnonymousLogger().log(Level.INFO, "Equipping " + item.getName() + " to boots slot.", this.hashCode());
        if (item.getSlot() != Item.Slot.FEET) {
            Logger.getAnonymousLogger().log(Level.INFO, "Cannot equip " + item.getName() + " to boots slot.", this.hashCode());
        }
        item_boots = item;
    }

    public void setGloves(Item item) {
        Logger.getAnonymousLogger().log(Level.INFO, "Equipping " + item.getName() + " to hands slot.", this.hashCode());
        if (item.getSlot() != Item.Slot.HANDS) {
            Logger.getAnonymousLogger().log(Level.INFO, "Cannot equip " + item.getName() + " to hands slot.", this.hashCode());
        }
        item_gloves = item;
    }

    public Item getHelm() {
        return item_headGear;
    }

    public Item getArmor() {
        return item_armor;
    }

    public Item getGloves() {
        return item_gloves;
    }

    public Item getBoots() {
        return item_boots;
    }

    public Stats getStats() {
        return stats;
    }

    public String getEmail() {
        return email;
    }

    public String getUserName() {
        return userName;
    }

    public String toString() {
        return "Player{" + "id=" + getId() + ", stats=" + stats + ", user name=" + userName + ", email=" + email + '}';
    }
}
