package warlox.armory.core;


import javax.persistence.Entity;
import javax.persistence.OneToOne;


@Entity
public class PlayerMatchStats extends AbstractEntity {
    @OneToOne
    private Player player;
    private int kills;
    private int deaths;
    private int assists;

    public PlayerMatchStats() {
    }

    public PlayerMatchStats(Player player, int kills, int deaths, int assists) {
        this.player = player;
        this.kills = kills;
        this.deaths = deaths;
        this.assists = assists;
    }
    
    public PlayerMatchStats(Long id, Player p, int kills, int deaths, int assists) {
        super(id);
        this.player = p;
        this.kills = kills;
        this.deaths = deaths;
        this.assists = assists;
    }

    public Player getPlayer() {
        return player;
    }

    public int getKills() {
        return kills;
    }
    public int getDeaths() {
        return deaths;
    }
    public int getAssists() {
        return assists;
    }
    @Override
    public String toString() {
        return "PlayerMatchStats{" + "player=" + player + '}';
    }
}
