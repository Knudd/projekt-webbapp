package warlox.armory.core;

import warlox.armory.db.AbstractDAO;
import java.util.ArrayList;
import java.util.List;

/**
 * All items
 */
public final class ItemCatalogue extends AbstractDAO<Item, Long>
        implements IItemCatalogue {
    
    public static IItemCatalogue newInstance(String puName){
        return new ItemCatalogue(puName);
    }
    
    public ItemCatalogue(String puName) {
        super(Item.class,puName);
    }

    @Override
    public List<Item> getByName(String name) {
        List<Item> found = new ArrayList<>();
        for (Item p : getRange(0, getCount())) {
            if (p.getName().equals(name)) {
                found.add(p);
            }
        }
        return found;
    }
    @Override
    public void removeAll(){
        for (Item p : getRange(0, getCount())) {
            remove(p.getId());
        }
    }

    @Override
    public Item getByNameAndSlot(String name, Item.Slot slot) {
        for (Item p : getRange(0, getCount())) {
            if (p.getName().equals(name) && p.getSlot() == slot) {
                return p;
            }
        }
        return null;
    }
    
}
