
package warlox.armory.core;

import warlox.armory.db.IDAO;
import java.util.List;

/**
 * Interface to Item catalogue
 */
public interface IItemCatalogue extends IDAO<Item, Long> {

    public List<Item> getByName(String name);

    public Item getByNameAndSlot(String name, Item.Slot slot);
     
}
