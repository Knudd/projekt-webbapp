package warlox.armory.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * An Item
 */
@Entity
public class Item extends AbstractEntity {

    public enum Slot {
        HEAD,
        CHEST,
        HANDS,
        FEET,
    }
    @Column(name = "item_name", nullable = false)
    private String name;
    private String img_name;
    private int damage;
    private int armor;
    private int speed;
    private int haste;
    @Enumerated(EnumType.STRING)
    private Slot slot;

    public Item() {
    }

    public Item(String name, Slot slot, String imgName, int dmg, int armor, int speed, int haste) {
        this.name = name;
        this.slot = slot;
        this.img_name = imgName;
        this.damage = dmg;
        this.armor = armor;
        this.speed = speed;
        this.haste = haste;
    }

    public Item(Long id, String name, Slot slot, String imgName, int dmg, int armor, int speed, int haste) {
        this(name, slot, imgName, dmg, armor, speed, haste);
        this.id = id;
    }

    public String getImg_name() {
        return img_name;
    }

    public String getName() {
        return name;
    }

    public int getDamage() {
        return damage;
    }

    public int getArmor() {
        return armor;
    }

    public int getHaste() {
        return haste;
    }

    public int getSpeed() {
        return speed;
    }

    public Slot getSlot() {
        return slot;
    }

    @Override
    public String toString() {
        return "Item{" + "id=" + getId() + ", name=" + name + ", slot=" + slot + '}';
    }
}
