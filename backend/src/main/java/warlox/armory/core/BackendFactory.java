package warlox.armory.core;

/**
 * Static factory for our Backend
 * Inspired from the course workshops.
 */
public class BackendFactory {

    private BackendFactory() {
    }

    // If initTestData there will be some data to use
    public static IBackend getBackend(String persistentUnitName) {
        Backend s = new Backend(persistentUnitName);
        return s;
    }
}
