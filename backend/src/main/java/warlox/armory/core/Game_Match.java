package warlox.armory.core;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 * A Game_Match
 * This class represents a match, and it contains the information about 
 * the participating players, the results and the date.
 */
@Entity
public class Game_Match extends AbstractEntity {

    @OneToMany(cascade = {CascadeType.ALL})
    private List<PlayerMatchStats> stats;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date date = new Date();

    public Game_Match() {
    }

    public Game_Match(List<PlayerMatchStats> stats, IPlayerRegistry pr) {
        this.stats = stats;
        int highestScore = Integer.MIN_VALUE;
        // Finds the highests score
        for (PlayerMatchStats ps : stats) {
            ps.getPlayer().getStats().addKills(ps.getKills());
            ps.getPlayer().getStats().addAssists(ps.getAssists());
            ps.getPlayer().getStats().addDeaths(ps.getDeaths());
            int score = ps.getKills() + ps.getAssists() - ps.getDeaths();
            if (score > highestScore) {
                highestScore = score;
            }
        }
        //Set the winner
        for (PlayerMatchStats ps : stats) {
            int score = ps.getKills() + ps.getAssists() - ps.getDeaths();
            if (score == highestScore) {
                ps.getPlayer().getStats().addWins(1);
                ps.getPlayer().getStats().addRating(15);
            } else {
                ps.getPlayer().getStats().addLosses(1);
            }
            pr.update(ps.getPlayer());
        }
    }

    @Override
    public String toString() {
        return "Match{" + "id=" + getId() + '}';
    }

    public Date getDate() {
        return date;
    }

    public List<PlayerMatchStats> getPlayerStats() {
        return stats;
    }

    public boolean containsPlayer(Player p) {
        for (PlayerMatchStats pms : stats) {
            if (pms.getPlayer().equals(p)) {
                return true;
            }
        }
        return false;
    }
}
