package warlox.armory.core;

/**
 * Backend is a container for other containers
 * These containers contain the database tables.
 */
public class Backend implements IBackend {

    private IItemCatalogue itemCatalogue;
    private IPlayerRegistry playerRegistry;
    private IMatchRegistry matchRegistry;

    public Backend(String puName) {
        itemCatalogue = ItemCatalogue.newInstance(puName);
        playerRegistry = PlayerRegistry.newInstance(puName);
        matchRegistry = MatchRegistry.newInstance(puName);
    }

    @Override
    public IPlayerRegistry getPlayerRegistry() {
        return playerRegistry;
    }

    @Override
    public IItemCatalogue getItemCatalogue() {
        return itemCatalogue;
    }

    @Override
    public IMatchRegistry getMatchRegistry() {
        return matchRegistry;
    }
}
