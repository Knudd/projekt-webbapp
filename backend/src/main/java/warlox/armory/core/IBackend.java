package warlox.armory.core;


/**
 * Public interface for the backend
 */
public interface IBackend{

    public IPlayerRegistry getPlayerRegistry();

    public IItemCatalogue getItemCatalogue();

    public IMatchRegistry getMatchRegistry();
}
