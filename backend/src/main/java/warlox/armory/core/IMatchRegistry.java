package warlox.armory.core;

import warlox.armory.db.IDAO;
import java.util.List;

/**
 * Interface to Match registry
 */
public interface IMatchRegistry extends IDAO<Game_Match, Long> {
    List<Game_Match> getByPlayer(Player player, int maxGames);    
}
