package warlox.armory.core;

import warlox.armory.db.IDAO;
import java.util.List;

/**
 * Interface to Player registry
 */
public interface IPlayerRegistry extends IDAO<Player, Long> {

    List<Player> getByName(String name);
    
}
