package warlox.armory.core;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * Various stats for a registered player
 */
@Embeddable
public class Stats implements Serializable {

    private int rating;
    private int wins;
    private int losses;
    private int kills;
    private int deaths;
    private int assists;

    public Stats() {
    }

    public Stats(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return "Stats{" + "rating=" + rating + '}';
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public int getKills() {
        return kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getAssists() {
        return assists;
    }

    public void addWins(int newWins) {
        wins += newWins;
    }

    public void addLosses(int newLosses) {
        losses += newLosses;
    }

    public void addKills(int newKills) {
        kills += newKills;
    }

    public void addAssists(int newAssists) {
        assists += newAssists;
    }

    public void addDeaths(int newDeaths) {
        deaths += newDeaths;
    }

    public void addRating(int rating) {
        this.rating += rating;
        if (this.rating < 0) {
            this.rating = 0;
        }
    }
}
