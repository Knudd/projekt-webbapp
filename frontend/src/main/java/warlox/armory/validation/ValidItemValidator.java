/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package warlox.armory.validation;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import warlox.armory.core.Item;

/*
 * Is not used by the frontend yet.
 */
public class ValidItemValidator implements ConstraintValidator<ValidItem, String> {
    Item.Slot slot;
    
    @Override
    public void initialize(ValidItem constraintAnnotation) {
        slot = constraintAnnotation.slot();
    }
 
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        //TODO Check if value is a correct itemname for slot "slot".
        return true;
    }
}