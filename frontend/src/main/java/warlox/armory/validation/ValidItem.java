package warlox.armory.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import warlox.armory.core.Item;

/*
 * Is not used by the frontend yet.
 */
@Constraint(validatedBy = ValidItemValidator.class)
public @interface ValidItem {
 
    String message() default "Not a valid item.";
 
    Class<?>[] groups() default {};
 
    Class<? extends Payload>[] payload() default {};
 
    Item.Slot slot() default Item.Slot.HEAD;
}
