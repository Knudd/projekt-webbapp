package warlox.armory.mb;

import java.io.Serializable;
import javax.inject.Singleton;
import warlox.armory.core.BackendFactory;
import warlox.armory.core.IBackend;
import warlox.armory.core.IPlayerRegistry;

@Singleton
public class BackendBean implements Serializable {

    private transient final IBackend s;
    final static String PU = "warlox_pu";

    private BackendBean() {
        s = BackendFactory.getBackend(PU);
    }

    public IPlayerRegistry getPlayerRegistery() {
        return s.getPlayerRegistry();
    }

    public IBackend getBackend() {
        return s;
    }
}
