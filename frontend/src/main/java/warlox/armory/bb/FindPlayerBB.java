package warlox.armory.bb;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import warlox.armory.mb.BackendBean;
import warlox.armory.core.Backend;
import warlox.armory.core.Player;
import warlox.armory.core.PlayerRegistry;


@Named("findplayer")
@RequestScoped
public class FindPlayerBB implements Serializable {

    private Backend backend;
    @NotNull
    private String name;
    private List<Player> foundPlayers;
    private int playerFound; //0 = initial, 1 = player found, 2 = no player found.

    @Inject
    void setBackend(BackendBean sb) {
        this.backend = (Backend) sb.getBackend();
        name = "";
        playerFound = 0;
    }
    
    public void search() {
        PlayerRegistry players = (PlayerRegistry) backend.getPlayerRegistry();
        foundPlayers = players.getBySubName(name);
        if (foundPlayers.size() > 0){
            playerFound = 1;
        } else {
            playerFound = 2;
        }
        
    }
    
    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public int getPlayerFound() {
        return playerFound;
    }
    
    public List<Player> getFoundPlayers() {
        if (foundPlayers == null)
            return null;
        return foundPlayers.subList(0,Math.min(foundPlayers.size(),15));
    }
}
