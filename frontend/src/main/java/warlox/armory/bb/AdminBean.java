package warlox.armory.bb;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/*
 * This bean is responsible handling
 * the dynamic pages in the admin section.
 * See admin.xhtml!
 */
@ManagedBean(name ="adminbean")
@ViewScoped
public class AdminBean implements Serializable {

    // What page should be included into the admin pages?
    private String page = "index";
    
    public String getPage(){
        return page;
    }
    
    public void setPage(String page){
        this.page = page;
    }
}
