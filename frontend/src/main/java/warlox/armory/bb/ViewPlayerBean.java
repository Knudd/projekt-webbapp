package warlox.armory.bb;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/*
 * This bean is responsible handling
 * the dynamic pages in the viewplayer section.
 * See viewPlayer.xhtml!
 */
@ManagedBean(name ="viewplayerbean")
@ViewScoped
public class ViewPlayerBean implements Serializable {

    private String page = "viewPlayerInfo";
    
    public String getPage(){
        return page;
    }
    
    public void setPage(String page){
        this.page = page;
    }
}
