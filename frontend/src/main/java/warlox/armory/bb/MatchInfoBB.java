package warlox.armory.bb;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import warlox.armory.mb.BackendBean;
import warlox.armory.core.Backend;
import warlox.armory.core.Game_Match;
import warlox.armory.core.PlayerMatchStats;


@Named("matchInfo")
@RequestScoped
public class MatchInfoBB implements Serializable {

    private Backend backend;

    @Inject
    void setBackend(BackendBean sb) {
        this.backend = (Backend) sb.getBackend();
    }
    
    public List<PlayerMatchStats> getPlayerInfo(String matchId) {
        Long mId = Long.parseLong(matchId);
        Game_Match match = backend.getMatchRegistry().find(mId);
        return match.getPlayerStats();
    }
}
