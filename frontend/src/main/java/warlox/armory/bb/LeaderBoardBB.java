package warlox.armory.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import warlox.armory.core.Player;

@Named("leaderboard")
@RequestScoped 
public class LeaderBoardBB implements Serializable {

    private EntityManagerFactory emf;
    private List<Player> playerList;
    private List<Player> tempList;
    int sIndex;

    public void setsIndex(int sIndex) {
        this.sIndex = sIndex;
    }

    public int getsIndex() {
        return sIndex;
    }
    int range = 20;
    final static String PU = "warlox_pu";

    @PostConstruct
    public void post() {
        // We know all injection are done so shop not null (hopefully)
        playerList = new ArrayList<Player>();
        tempList = new ArrayList<Player>();
        emf = Persistence.createEntityManagerFactory(PU);
    }

    public List<Player> getRange(List<Player> list, int start) {
        tempList.clear();
        for (int i = start; i <= range + start; i++) {
            if (i >= playerList.size()) {
                return tempList;
            } else {
                tempList.add(list.get(i));
            }
        }
        return tempList;
    }

    public String next(String start) {
        if (start.equals("")) {
            start = "0";
        }
        sIndex = Integer.parseInt(start);
        if (!(sIndex + range > playerList.size())) {
            sIndex += range;
        }
        return String.valueOf(sIndex);
    }

    public String prev(String start) {
        if (start.equals("")) {
            start = "0";
        }
        sIndex = Integer.parseInt(start);
        if (sIndex - range < 0) {
            sIndex = 0;
        } else {
            sIndex -= range;
        }
        return String.valueOf(sIndex);
    }

    public List<Player> sortByQuery(String namedQuery) {
        playerList.clear();
        EntityManager em = emf.createEntityManager();
        Query query = em.createNamedQuery(namedQuery);
        playerList.addAll(query.getResultList());
        return playerList;
    }
    /*
     *This method returns a list of players, decided if ascending or descending, 
     *if its sorted by rating or name.
     *And what index we want the list to start at.
     */
    public List<Player> getPlayers(String order, String start, String namedQuery) {
        System.out.println("prev: " + sIndex);
        List<Player> toReturn;
        int startAt = 0;
        if (!start.equals("")) {
            startAt = Integer.parseInt(start);
        }
        if (order != null && namedQuery != null) {
            if (namedQuery.equals("name")) {
                toReturn = sortByQuery("Player.sortByName");
            } else if (namedQuery.equals("rating")) {
                toReturn = sortByQuery("Player.sortByRating");
            } else {
                toReturn = sortByQuery("Player.sortByRating");
            }
        } else {
            toReturn = playerList;
        }
        if (order.equals("ASC")) {
            //     Logger.getAnonymousLogger("").log(Level.INFO, "HEJ");
            Collections.reverse(toReturn);
        }

        toReturn = getRange(toReturn, startAt);
        return toReturn;
    }

    public String reverseOrder(String next, String current, String order) {
        if (!next.equals(current)) {
            return "DESC";
        }
        if (order.equals("DESC")) {
            return "ASC";
        } else {
            return "DESC";
        }
    }
}
