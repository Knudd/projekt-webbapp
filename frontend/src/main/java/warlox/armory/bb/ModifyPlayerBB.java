package warlox.armory.bb;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import warlox.armory.mb.BackendBean;
import warlox.armory.core.Backend;
import warlox.armory.core.Item;
import warlox.armory.core.Player;
import warlox.armory.core.PlayerRegistry;

@Named("modifyplayer")
@RequestScoped
public class ModifyPlayerBB implements Serializable {

    private Backend backend;
    @NotNull
    private String selectedPlayerName;
    private int status; // 0 = Initial, 1 = player found, 2 = no such player
    private String activePlayer;
    @NotNull
    private String helmName;
    @NotNull
    private String gloveName;
    @NotNull
    private String bootName;
    @NotNull
    private String chestName;

    @Inject
    void setBackend(BackendBean sb) {
        this.backend = (Backend) sb.getBackend();
        selectedPlayerName = "";
        activePlayer = "";
        status = 0;
    }

    public void searchByName() {
        PlayerRegistry players = (PlayerRegistry) backend.getPlayerRegistry();
        List<Player> foundPlayers = players.getByName(selectedPlayerName);
        activePlayer = selectedPlayerName;
        if (foundPlayers.size() > 0) {
            status = 1;
            Player p = foundPlayers.get(0);
            helmName = p.getHelm().getName();
            gloveName = p.getGloves().getName();
            chestName = p.getArmor().getName();
            bootName = p.getBoots().getName();
        } else {
            status = 2;
        }
    }

    public void setSelectedPlayerName(String name) {
        this.selectedPlayerName = name;
    }

    public String getSelectedPlayerName() {
        return selectedPlayerName;
    }

    public int getStatus() {
        return status;
    }

    public String save() {
        Player updatedPlayer = backend.getPlayerRegistry().getByName(selectedPlayerName).get(0);
        Item chest = backend.getItemCatalogue().getByNameAndSlot(chestName, Item.Slot.CHEST);
        Item helm = backend.getItemCatalogue().getByNameAndSlot(helmName, Item.Slot.HEAD);
        Item gloves = backend.getItemCatalogue().getByNameAndSlot(gloveName, Item.Slot.HANDS);
        Item boots = backend.getItemCatalogue().getByNameAndSlot(bootName, Item.Slot.FEET);
        updatedPlayer.setArmor(chest);
        updatedPlayer.setHelm(helm);
        updatedPlayer.setGloves(gloves);
        updatedPlayer.setBoots(boots);
        backend.getPlayerRegistry().update(updatedPlayer);
        return "admin?faces-redirect=true";
    }

    public String getHelmName() {
        return helmName;
    }

    public void setHelmName(String helmName) {
        this.helmName = helmName;
    }

    public String getGloveName() {
        return gloveName;
    }

    public void setGloveName(String gloveName) {
        this.gloveName = gloveName;
    }

    public String getBootName() {
        return bootName;
    }

    public void setBootName(String bootName) {
        this.bootName = bootName;
    }

    public String getChestName() {
        return chestName;
    }

    public void setChestName(String chestName) {
        this.chestName = chestName;
    }
}
