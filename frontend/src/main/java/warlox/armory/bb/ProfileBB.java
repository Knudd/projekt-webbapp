package warlox.armory.bb;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.inject.Inject;
import warlox.armory.mb.BackendBean;
import warlox.armory.core.Backend;
import warlox.armory.core.Game_Match;
import warlox.armory.core.Item;
import warlox.armory.core.Player;


@ManagedBean(name = "playerProfile")
@RequestScoped
public class ProfileBB implements Serializable {

    private Backend backend;
    @ManagedProperty(value = "#{param.id}")
    private Long id; // +setter

    public void setId(Long i) {
        id = i;
    }

    @Inject
    void setBackend(BackendBean sb) {
        this.backend = (Backend) sb.getBackend();
    }

    public List<Game_Match> getMatches() {
        Player player = backend.getPlayerRegistry().find(id);
        List<Game_Match> games = backend.getMatchRegistry().getByPlayer(player, 5);
        return games;
    }

    public String getName() {
        Player player = backend.getPlayerRegistry().find(id);
        if (player == null) {
            return "";
        }
        return player.getUserName();
    }

    public Player getPlayer() {
        Player player = backend.getPlayerRegistry().find(id);
        if (player == null) {
            return null;
        }
        return player;
    }

    public String getRating() {
        Player player = backend.getPlayerRegistry().find(id);
        if (player == null) {
            return "";
        }
        return Integer.toString(player.getStats().getRating());
    }

    public Item getHelm() {
        Player player = backend.getPlayerRegistry().find(id);
        if (player == null) {
            return null;
        }
        return player.getHelm();
    }

    public Item getArmor() {
        Player player = backend.getPlayerRegistry().find(id);
        if (player == null) {
            return null;
        }
        return player.getArmor();
    }

    public Item getGloves() {
        Player player = backend.getPlayerRegistry().find(id);
        if (player == null) {
            return null;
        }
        return player.getGloves();
    }

    public Item getBoots() {
        Player player = backend.getPlayerRegistry().find(id);
        if (player == null) {
            return null;
        }
        return player.getBoots();
    }

    public String getColor(String percent) {
        if (percent == null) {
            return "";
        }
        int number = Integer.parseInt(percent);
        if (number == 0) {
            return "#0000FF";
        } else if (number < 0) {
            return "#FF0000";
        } else {
            return "#00FF00";
        }
    }

    public String getTotalDamage() {
        int totalDamage = getHelm().getDamage() + getArmor().getDamage() + getGloves().getDamage() + getBoots().getDamage();
        return Integer.toString(totalDamage);
    }

    public String getTotalArmor() {
        int totalArmor = getHelm().getArmor() + getArmor().getArmor() + getGloves().getArmor() + getBoots().getArmor();
        return Integer.toString(totalArmor);
    }

    public String getTotalCooldownReduction() {
        int totalCdRed = getHelm().getHaste() + getArmor().getHaste() + getGloves().getHaste() + getBoots().getHaste();
        return Integer.toString(totalCdRed);
    }

    public String getTotalMovementSpeed() {
        int totalMS = getHelm().getSpeed() + getArmor().getSpeed() + getGloves().getSpeed() + getBoots().getSpeed();
        return Integer.toString(totalMS);
    }
}
