$(document).ready(function() {
    $('.tooltip').hide();
    $('.trigger').mouseover(function() {
         $this=$(this),
         $tip = $($this.attr('data-tooltip'));
         $tip.fadeIn(1);
         $(document).mousemove(function(e){
             
             var left = e.clientX+10;
             var bottom = window.innerHeight-e.clientY;
             var bottom = bottom - $tip.height()-50;
             if ((left+250) > window.innerWidth) {
                 left = window.innerWidth-250;
             }
             if (bottom < 0) {
                 bottom = 0;
             }
             $tip.css({left: left, bottom: bottom});
         });
    }); // end mouseover
    $('.trigger').mouseout(function () {
         $('.tooltip').fadeOut(1);
    }); // end mouseout    
});