package warlox.armory.tests;

import warlox.armory.testhelpers.JavaBeanTester;
import warlox.armory.bb.AddPlayerBB;
import warlox.armory.bb.AuthBB;
import warlox.armory.bb.LeaderBoardBB;
import warlox.armory.bb.MatchInfoBB;
import warlox.armory.bb.ProfileBB;
import java.beans.IntrospectionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import warlox.armory.bb.AdminBean;
import warlox.armory.mb.BackendBean;
import org.junit.Test;
import warlox.armory.bb.FindPlayerBB;
import warlox.armory.bb.ModifyPlayerBB;

/**
 * Tests for the frontend
 *
 */
public class TestFrontEnd {

    @Test
    public void testBeans() {
        try {
            JavaBeanTester.test(AddPlayerBB.class);
            JavaBeanTester.test(ModifyPlayerBB.class);
            JavaBeanTester.test(FindPlayerBB.class);
            JavaBeanTester.test(AuthBB.class);
            JavaBeanTester.test(LeaderBoardBB.class);
            JavaBeanTester.test(MatchInfoBB.class);
            JavaBeanTester.test(ProfileBB.class);
            JavaBeanTester.test(AdminBean.class);
            JavaBeanTester.test(BackendBean.class);
            
        } catch (IntrospectionException ex) {
            Logger.getLogger(TestFrontEnd.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
